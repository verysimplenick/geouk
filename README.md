# Latest Stable Version #
The source within the master branch represents the latest stable version and reflects the version that is published in Nuget.org.

# GeoUK #
The project allows for a conversion from GPS coordinates to British National Grid and back again. The product is available as a NuGet package (GeoUK). GeoUK was built as a Portable Class Library project (Profile 78) and as such can be added to Visual Studio or Xamarin and runs against .Net 4.5+, Windows Phone 8+, Xamarin.IOS/Android, Windows 8 Store Apps and so on. The product is licensed under the [GNU Lesser General Public License (LGPL)](https://www.gnu.org/licenses/lgpl-3.0.en.html).

# GeoUK.OSTN #
The GeoUk.OSTN project, also available as a NuGet package (GeoUK.OSTN) Adds OSTN02 and OSTN15 transformation which provide a greater accuracy. A full description of how to use this package can be found in the CodeProject article [Converting Latitude and Longitude to British National Grid in C#](http://www.codeproject.com/Articles/1007147/Converting-Latitude-and-Longitude-to-British-Natio)